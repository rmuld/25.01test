package ee.bcs.valiit;

import java.util.ArrayList;

public class Main {


    public static void main(String[] args) {

        /*
         *Ülesanne 1:
         *Deklareeri muutuja, mis hoiaks endas Pi (https://en.wikipedia.org/wiki/Pi) arvulist väärtust vähemalt 11 kohta peale koma.
         *Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.

         */

        double pi = 3.14159265358;
        System.out.println(pi * 2);


        /*Ülesanne 2
         * */
        int num1;
        int num2;
        Ülesanne2 y2 = new Ülesanne2();
        y2.arvutame();
        System.out.println();

        /*Ülesanne 3
         * */
        Ülesanne3 y3 = new Ülesanne3();
        y3.nimekiri();


        /*Ülesanne 4
         * */
        Ülesanne4 y4 = new Ülesanne4();
        y4.aastaarvud();
        System.out.println();

        /*Ülesanne 5
         * */
        Country y5 = new Country();
        y5.riigid();
        System.out.println();



    }


}

