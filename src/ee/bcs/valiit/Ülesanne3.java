package ee.bcs.valiit;

import java.util.ArrayList;

public class Ülesanne3 {
    /*Ülesanne 3: Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja mis tagastab täisarvude massiivi.
     * Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi (stringi) pikkust.
     * Meetodi nime võid ise välja mõelda.
     * Kutsu see meetod main()-meetodist välja ja prindi tulemus (kõik massiivi elemendid) standardväljundisse.
     * */

    public void nimekiri() {
        String[] names = {"Siirile", "Viivikene", "Sille", "Pill"};


        for (String name : names) {
            System.out.println(name);

        }
        for (int i = 0; i < 5; i++) {
            System.out.printf("NIMI: %s, nime pikkus %s\n",names[i],  names[i].length());
        }

    }
}
