package ee.bcs.valiit;

public class Ülesanne2 {
    /*Ülesanne 2:
     *Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks täisarvulist muutujat.
     * Meetod tagastab tõeväärtuse vastavalt sellele, kas kaks sisendparametrit on omavahel võrdsed või mitte.
     * Meetodi nime võid ise välja mõelda.
     *Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.
     * */

        public static void arvutame(){
            int num1 = 2;
            int num2 = 2;
            if(num1 == num2){
                System.out.println("True");

            }else {
                System.out.println("False");
            }
        }

}
